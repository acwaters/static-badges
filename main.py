#
#                         Copyright (C) 2015 A Connor Waters
#
#             Distributed under the Boost Software License, Version 1.0.
#   (See accompanying file LICENSE or copy at http://www.boost.org/LICENSE_1_0.txt)
#

from flask import Flask, send_file
from github import Github
from StringIO import StringIO

import genimg

app = Flask(__name__)

@app.route('/github/<ghuser>.png')
def ghbadge(ghuser):
    gh = Github()
    user = gh.get_user(ghuser)
    img = genimg.genimg(user)
    tmp = StringIO()
    img.save(tmp, 'PNG')
    tmp.seek(0)
    return send_file(tmp, mimetype = 'image/png')
