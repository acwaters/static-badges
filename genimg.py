#
#                         Copyright (C) 2015 A Connor Waters
#
#             Distributed under the Boost Software License, Version 1.0.
#   (See accompanying file LICENSE or copy at http://www.boost.org/LICENSE_1_0.txt)
#

from PIL import Image, ImageDraw, ImageFont
from urllib import urlopen

def genimg(user):
    img = Image.new('RGB', (600, 400), 'white')
    avi = Image.open(urlopen(user.avatar_url)).resize((120,120), Image.ANTIALIAS)
    img.paste(avi,(32,32))
    draw = ImageDraw.Draw(img)
    head = ImageFont.truetype('font/texgyreheros-regular.otf', 28)
    text = ImageFont.truetype('font/texgyreheros-regular.otf', 18)
    draw.text((184,32), user.name or user.login, font=head, fill='black')
    draw.text((184,72), user.location or '', font=text, fill='dimgray')
    draw.text((184,96), user.email or '', font=text, fill='dimgray')
    draw.text((184,120), 'Joined GitHub on '
        + user.created_at.strftime('%d %B %Y'), font=text, fill='dimgray')
    del draw
    return img
